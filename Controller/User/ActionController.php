<?php

namespace PiZone\UserBundle\Controller\User;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\UserBundle\Entity\User';
        $this->repository = 'PiZone\UserBundle\Entity\User';
        $this->route['delete'] = 'default_user_delete';
        $this->route['list']['name'] = 'default_user';
    }
}