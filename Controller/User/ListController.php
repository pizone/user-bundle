<?php

namespace PiZone\UserBundle\Controller\User;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use PiZone\CatalogBundle\PiZoneCatalogBundle;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\UserBundle\UserList';
        $this->routeList = array(
            'list' => array(
                'name' => 'default_user',
                'parameters' => array()
            ),
            'delete' => 'default_user_delete',
            'batch' => array(
                'delete' => 'default_user_batch_delete',
                'active' => 'default_user_batch_active'
            )
        );
        $this->model = 'PiZone\UserBundle\Entity\User';
        $this->repository = 'PiZone\UserBundle\Entity\User';
        $this->filtersForm = 'PiZone\UserBundle\Form\UserFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'username' => $one->getUsername(),
                'email' => $one->getEmail(),
                'enabled' => $one->isEnabled(),
                'registration_at' => $one->getRegistrationAt() ? $one->getRegistrationAt()->format('d.m.Y H:i'): '',
                'last_login' => $one->getLastLogin() ? $one->getLastLogin()->format('d.m.Y H:i'): '',
                'roles' => $one->getRoles(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('username', $filterObject['all']);
            $queryFilter->addOrStringFilter('email', $filterObject['all']);
        }
        if (isset($filterObject['username']) && null !== $filterObject['username']) {
            $queryFilter->addStringFilter('username', $filterObject['username']);
        }
        if (isset($filterObject['email']) && null !== $filterObject['email']) {
            $queryFilter->addStringFilter('email', $filterObject['email']);
        }
        if (isset($filterObject['is_enabled']) && null !== $filterObject['is_enabled']) {
            $queryFilter->addBooleanFilter('is_enabled', $filterObject['is_enabled']);
        }
        if (isset($filterObject['registration_at']) && null !== $filterObject['registration_at']) {
            $this->addPublishFilter($queryFilter, $filterObject['registration_at']);
        }
        if (isset($filterObject['last_login']) && null !== $filterObject['last_login']) {
            $this->addPublishFilter($queryFilter, $filterObject['last_login']);
        }
    }
}