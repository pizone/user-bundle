<?php

namespace PiZone\UserBundle\Controller\User;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

/**
 * Account controller.
 *
 */
class AccountController extends FOSRestController
{
    public function infoAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $result = array(
            'username' => $user->getUsername()
        );
        $view = $this->view($result)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }
}
