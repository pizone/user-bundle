<?php

namespace PiZone\UserBundle\Controller\User;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;
use Symfony\Component\HttpFoundation\Request;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\UserBundle\Entity\User';
        $this->form = 'PiZone\UserBundle\Form\UserType';
        $this->routeList['update'] = 'default_user_update';
        $this->routeList['delete'] = 'default_user_delete';
    }

    protected function save($em, $user, $form){
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);
    }
}