<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PiZone\UserBundle\Controller\Security;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends FOSRestController
{
    protected $request;
    protected $requestInvalid = false;

    protected $requestInvalidMessages = array();

    public function loginAction(Request $request)
    {
        $this->request = $request;
        $view = new View();

        if(!$this->getUser()) {
            $username = $this->getParameter('username');
            $password = $this->getParameter('password');

            // Validating parameters
            if ($this->requestInvalid) {
                $view->setStatusCode(Codes::HTTP_BAD_REQUEST);
                $view->setData(array(
                    'result' => 'error',
                    'message' => $this->requestInvalidMessages
                ));
                return $this->handleView($view);
            }

            $user = $this->get('pi_zone.user_manager')->findUserByUsernameOrEmail($username);

            if (!$user) {
                $view->setStatusCode(Codes::HTTP_FORBIDDEN);
                $view->setData(array(
                    'result' => 'error',
                    'message' => 'You do not have the necessary permissions'
                ));
                return $this->handleView($view);
            }
            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $encoded_pass = $encoder->encodePassword($password, $user->getSalt());

            if ($user->getPassword() != $encoded_pass) {
                $view->setStatusCode(Codes::HTTP_FORBIDDEN);
                $view->setData(array(
                    'result' => 'error',
                    'message' => 'You do not have the necessary permissions'
                ));
                return $this->handleView($view);
            }
        }
        else{
            $user = $this->getUser();
        }

        $token = new UsernamePasswordToken($user, null, "main", $user->getRoles());
        $this->get('security.token_storage')->setToken($token); //now the user is logged in
        //now dispatch the login event
        $event = new InteractiveLoginEvent($this->request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $view->setData(array(
            'result' => 'ok',
            'roles' => $this->getFullRoles($user->getRoles())
        ));

        return $this->handleView($view);
    }

    private function getFullRoles($roles){
        $full =  $this->container->getParameter('security.role_hierarchy.roles');
        $list = array();
        foreach($roles as $one){
            $list[] = $one;
            if(isset($full[$one])) {
                foreach($full[$one] as $one2){
                    $list[] = $one2;
                }
            }
        }

        return array_unique($list);
    }

    public function logoutAction(Request $request)
    {
        $view = new View();
        try {
            $request->getSession()->invalidate();
            $this->get('security.token_storage')->setToken(null);
            $view->setData(array(
                'result' => 'ok'
            ));
        } catch (\Exception $e) {
            $view->setStatusCode(Codes::HTTP_INTERNAL_SERVER_ERROR);
            $view->setData(array(
                'result' => 'error',
                'message' => $e->getMessage()
            ));
        }
        return $this->handleView($view);
    }

    protected function getParameter($key, $optional = false, $message = null, $default = null){
        $parameter = $this->request->get($key, $default);

        if (!$optional && is_null($parameter)) {
            if (is_null($message)) {
                $message = $key . ' parameter is required';
            }
            $this->addRequestInvalidMessage($key, $message);
        }

        return $parameter;
    }

    protected function addRequestInvalidMessage($key, $message){
        $this->requestInvalid = true;
        if ($message != '')
            $this->requestInvalidMessages[$key] = $message;
    }
}
