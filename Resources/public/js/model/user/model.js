function AdminDefaultUser($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/user';
    self.statelink.edit = 'user.default_user_edit';

    angular.extend(this, self);
}