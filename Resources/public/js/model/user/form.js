function AdminDefaultUserForm($scope) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){
        $scope.breadcrumbs.param = data.username.value;

        var requiredMess = 'USER.MESSAGE.REQUIRED.',
            regexpMess = 'USER.MESSAGE.REGEXP.';

        data.plainPassword.form.first.type = 'password';
        data.plainPassword.form.second.type = 'password';

        data.roles.template = FieldDispatcher.GetLayout('multiselect');
        data.roles.type = 'multiselect';


        $scope.tabs = [
                {
                    title: 'USER.TAB.GENERAL.TITLE',
                    description: '',
                    form: true,
                    valid: true,
                    groups: [
                        [
                            {size: 0, icon: '', field: data._token},
                            {
                                size: 6, icon: 'fa fa-user', field: data.username, assert: [
                                {key: 'notNull', message: requiredMess + 'USERNAME'}
                            ]
                            },
                            {
                                size: 6, icon: 'fa fa-envelope', field: data.email, assert: [
                                {key: 'notNull', message: requiredMess + 'EMAIL'},
                                {key: 'email', message: regexpMess + 'EMAIL'}
                            ]
                            }
                        ],
                        [
                            {
                                size: 6, icon: 'fa fa-lock', field: data.plainPassword.form.first, assert: [
                                    // {key: 'notNull', message: requiredMess + 'PASSWORD'},
                                    {
                                        key: 'repeat',
                                        message: 'USER.MESSAGE.REPEAT.PASSWORD',
                                        compare: data.plainPassword.form.second
                                    }
                                ]
                            },
                            {
                                size: 6, icon: 'fa fa-lock', field: data.plainPassword.form.second, assert: [
                                    // {key: 'notNull', message: requiredMess + 'PASSWORD'}
                                ]
                             }
                        ]
                    ]
                },
                {
                    title: 'USER.TAB.PERMISSIONS.TITLE',
                    description: '',
                    form: true,
                    valid: true,
                    groups: [
                        [
                            { size: 6, icon: 'fa fa-user', field: data.roles }
                        ]
                    ]
                }
            ];
    }
}