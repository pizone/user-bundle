function AuthInterceptor($q , $location, $sessionStorage, $cookieStore) {
    return {
        // Add authorization token to headers
        request: function (config) {
            config.headers = config.headers || {};
            if ($cookieStore.get('PHPSESSID')) {
                $sessionStorage.isAuthorized = true;
            }
            return config;
        },

        responseError: function(response) {
            if(response.status === 401 || !$sessionStorage.isAuthorized) {
                $location.path('/login');
                $cookieStore.remove('token');
            }
            else if (response.status === 403) {
                $location.path('/403');
            }
            else if (response.status === 404) {
                $location.path('/404');
            }
            else if (response.status === 500) {
                $location.path('/500');
            }

            return $q.reject(response);
        }
    };
}
angular
    .module('PiZone.User')
    .factory('authInterceptor', AuthInterceptor);


AuthInterceptor.$inject = ['$q', '$location', '$sessionStorage', '$cookieStore'];