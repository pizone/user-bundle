function UserTranslationConfig($translateProvider) {
    $translateProvider
        .translations('en', {
            USER: enUserPZTranslates,
            ACCOUNT: enAccountPZTranslates
        })
        .translations('ru', {
            USER: ruUserPZTranslates,
            ACCOUNT: ruAccountPZTranslates
        });
}
UserTranslationConfig.$inject = ['$translateProvider'];

angular
    .module('PiZone.User')
    .config(UserTranslationConfig);
