
function StateConfigPZUser($stateProvider, $ocLazyLoadProvider, IdleProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl:  "/bundles/pizoneuser/tmpl/security/login.html",
            data: { pageTitle: 'Login', specialClass: 'gray-bg' }
        })
        .state('user', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('user.default_user_new', GetStateConfig('user', 'new'))
        .state('user.default_user_edit', GetStateConfig('user', 'edit'))
        .state('user.default_user_list', GetStateConfig('user', 'list'))
    ;
    var collections = new LazyLoadCollection();

    function GetStateConfig(name, action){
        var url = '',
            tmpl = '',
            path = "/bundles/pizoneuser/tmpl/";
        if(action === 'list'){
            url = "/" + name + "/:pageId?sort&order_by&perPage";
            tmpl = path + name + '/index.html';
        }
        else if (action === 'new'){
            url = "/" + name + "/new";
            tmpl = path + name + '/new.html';
        }
        else if(action === 'edit'){
            url = "/" + name + "/edit/:id";
            tmpl = path + name + '/edit.html';
        }
        return {
                url: url,
                templateUrl: tmpl,
                resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    console.log(action);
                    if(action === 'list')
                        return $ocLazyLoad.load(collections.GetList());
                    else
                        return $ocLazyLoad.load(collections.GetEdit());
                }
            }
        };
    }
}
StateConfigPZUser.$inject = ['$stateProvider', '$ocLazyLoadProvider', 'IdleProvider'];

angular
    .module('PiZone.User')
    .config(StateConfigPZUser);
