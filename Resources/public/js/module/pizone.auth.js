(function () {
    'use strict';
    angular.module('PiZoneAuth', [])
        .service('SessionService', ['$injector', '$rootScope', '$sessionStorage', '$http', '$cookieStore', function ($injector, $rootScope, $sessionStorage, $http, $cookieStore) {
            var service = {};
            service.CheckAccess = CheckAccess;
            service.SetCredentials = SetCredentials;
            service.ClearCredentials = ClearCredentials;
            service.CheckCredentials = CheckCredentials;

            return service;

            function CheckAccess(event, toState, toParams, fromState, fromParams) {
                if (toState.data !== undefined) {
                    if (toState.data.noLogin !== undefined && toState.data.noLogin) {
                        return true;
                    }
                }
                else {
                    if (!$sessionStorage.isAuthorized) {
                        var $scope = $injector.get('$rootScope');
                        // если пользователь не авторизован - отправляем на страницу авторизации
                        event.preventDefault();
                        $scope.$state.$last = $scope.$state.current;
                        $scope.$state.go('login');
                        return true;
                    }
                }
            }

            function SetCredentials(username, password, roles) {
                $sessionStorage.isAuthorized = true;
                $cookieStore.put('roles', roles);
            }

            function CheckCredentials(role){
                var roles = $cookieStore.get('roles');
                if(!$.isArray(role)){
                    role = [role];
                }
                var test = false;

                for(var i = 0; i <= role.length-1; i++) {
                    if ($.inArray(role[i], roles) >= 0) {
                        test = true;
                    }
                }

                return test;
            }

            function ClearCredentials() {
                $rootScope.globals = {};
                $sessionStorage.isAuthorized = false;
                $cookieStore.remove('roles');
            }
        }]
    );
})();