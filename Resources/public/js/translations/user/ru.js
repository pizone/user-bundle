var ruUserPZTranslates = {
    LIST: 'Пользователи',
    ADMIN_LIST: 'Администраторы',
    BUYER_LIST: 'Покупатели',
    NEW: 'Новый пользователь',
    EDIT: 'Редактировать пользователя - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'Основное'
        },
        PERMISSIONS: {
            TITLE: 'Доступ'
        }
    },
    FIELD: {
        USERNAME: 'Название',
        EMAIL: 'Описание',
        FULL_NAME: 'Ф.И.О.',
        ENABLED: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        REGISTRATION_AT: 'Дата регистрации',
        LAST_LOGIN: 'Дата последнего входа',
        PASSWORD: 'Пароль',
        PASSWORD_CONFIRMATION: 'Подтвердите пароль',
        ROLES: 'Роли',
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            USERNAME: 'Введите имя пользователя.',
            EMAIL: 'Введите email.',
            PASSWORD: 'Введите пароль.'
        },
        REGEXP: {
            EMAIL: 'Строка не является адресом email'
        },
        REPEAT: {
            PASSWORD: 'Пароли не совпадают.'
        }
    }
};