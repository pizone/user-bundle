var enUserPZTranslates = {
    LIST: 'Users',
    ADMIN_LIST: 'Management',
    BUYER_LIST: 'Buyers',
    NEW: 'New user',
    EDIT: 'Edit user - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'General'
        },
        PERMISSIONS: {
            TITLE: 'Permissions'
        }
    },
    FIELD: {
        USERNAME: 'Username',
        EMAIL: 'Email',
        FULL_NAME: 'Full name',
        ENABLED: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        REGISTRATION_AT: 'Registration date',
        LAST_LOGIN: 'Last login date',
        PASSWORD: 'Password',
        PASSWORD_CONFIRMATION: 'Password confirmation',
        ROLES: 'Roles',
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            USERNAME: 'Please enter username.',
            EMAIL: 'Please enter email.',
            PASSWORD: 'Please enter password'
        },
        REGEXP: {
            EMAIL: 'No valid email'
        },
        REPEAT: {
            PASSWORD: 'Passwords do not match.'
        }
    }
};