var ruAccountPZTranslates = {
    USERNAME: 'Логин',
    PASSWORD: 'Пароль',
    LOGIN: 'Войти',
    ERROR_LOGIN: 'Неверный логин или пароль',
    REGISTRATION: 'Зарегистрироваться',
    REGISTRATION_MESSAGE: 'У Вас нет аккаунта?',
    WELCOME: 'Вас приветствует Wifinder',
    DESCRIPTION: 'ведущий системный интегратор в области силовых и слаботочных систем зданий и сооружений.',
    COPYRIGHT: 'Интерфейс приложения основан на Bootstrap 3 © 2014'
};