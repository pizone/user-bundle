var enAccountPZTranslates = {
    USERNAME: 'Username',
    PASSWORD: 'Password',
    LOGIN: 'Login',
    ERROR_LOGIN: 'Login name or password is incorrect',
    REGISTRATION: 'Create an account',
    REGISTRATION_MESSAGE: 'Do not have an account?',
    WELCOME: 'Welcome to Wifinder',
    DESCRIPTION: 'the leading system integrator in the field of security and low-voltage systems of buildings and structures.',
    COPYRIGHT: 'The application interface is base on Bootstrap 3 © 2014'
};