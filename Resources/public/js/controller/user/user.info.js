function UserInfoCtrl($scope, $http, SessionService, $cookieStore){
    $scope.logoutButtonId = 'logoutButtonId';
    $scope.username = '';
    $scope.avatar = PiZoneConfig.pathToImg + 'User-icon.png';
    $scope.showAdminLink = false;
    $scope.Click = {
        Logout: Logout
    };

    function Init(){
        GetInfo();
    }

    function GetInfo(){
        $http.get(PiZoneConfig.apiPrefix + '/account').success(function(data) {
            $scope.username = data.username;
            if(SessionService.CheckCredentials('ROLE_SUPER_ADMIN') || SessionService.CheckCredentials('ROLE_ADMIN'))
                $scope.showAdminLink = true;
            else
                $scope.showAdminLink = false;
        });
    }

    function Logout(){
        var container = $('#' + $scope.logoutButtonId);
        var loader = new Loader(container);
        $.ajax({
            url: PiZoneConfig.apiPrefix + '/logout',
            type: 'post',
            dataType: 'json',
            beforeSend: function(){
                if(loader.ready === false)
                    loader.Create();
            },
            error: function(message) {
                //PiZoneException.AjaxError(message, loader)
            },
            success: function(message){
                loader.Delete();
                if(message.result == 'ok') {
                    SessionService.ClearCredentials();
                    $scope.$state.go('login');
                }
                else{

                }

            }
        });
    }

    Init();
}
UserInfoCtrl.$inject = ['$scope', '$http','SessionService', '$cookieStore'];
angular.module('PiZone.User').controller('UserInfoCtrl', UserInfoCtrl);