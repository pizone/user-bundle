function AdminDefaultUserListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    var str = 'user.default_user_',
    field = 'USER.FIELD.';

    $scope.prefix = 'admin/user';
    $scope.statelink.add = str + 'new';
    $scope.statelink.edit = str + 'edit';
    $scope.statelink.page = str + 'list';
    $scope.breadcrumbs.title = 'USER.LIST';
    $scope.model = AdminDefaultUser;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: field + 'USERNAME', name: 'username'},
        {title: field + 'EMAIL', name: 'email'},
        {title: field + 'ENABLED.LABEL', name: 'enabled'},
        {title: field + 'REGISTRATION_AT', name: 'registration_at'},
        {title: field + 'LAST_LOGIN', name: 'last_login'},
        {title: field + 'ROLES'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5, 8]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.enabled.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;
        data.registration_at.type = 'date_range';
        data.registration_at.template = FieldDispatcher.GetLayout('date_range');
        data.last_login.type = 'date_range';
        data.last_login.template = FieldDispatcher.GetLayout('date_range');

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-user', field: data.username, show: data.username.value ? true : false},
            {icon: 'fa fa-envelope', field: data.email, show: data.email.value ? true : false},
            {icon: 'fa fa-clock', field: data.registration_at, show: $scope.ShowDateFilter(data.registration_at.form)},
            {icon: 'fa fa-clock', field: data.last_login, show: $scope.ShowDateFilter(data.last_login.form)},
            {icon: 'fa fa-eye', field: data.enabled, show: data.enabled.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminDefaultUserListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.User').controller('AdminDefaultUserListCtrl', AdminDefaultUserListCtrl);