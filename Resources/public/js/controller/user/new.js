function AdminDefaultUserNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    var str = 'user.default_user_';
    $scope.prefix = 'admin/user';
    $scope.statelink.list = str + 'list';
    $scope.statelink.new = str + 'new';
    $scope.statelink.edit = str + 'edit';
    $scope.breadcrumbs.title = 'USER.NEW';
    $scope.breadcrumbs.path = [{title: 'USER.LIST', url: str + 'list'}];

    var Form = new AdminDefaultUserForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminDefaultUserNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.User').controller('AdminDefaultUserNewCtrl', AdminDefaultUserNewCtrl);