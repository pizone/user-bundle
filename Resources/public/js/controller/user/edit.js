function AdminDefaultUserEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    var str = 'user.default_user_';
    $scope.prefix = 'admin/user';
    $scope.statelink.list = str + 'list';
    $scope.statelink.new = str + 'new';
    $scope.breadcrumbs.title = 'USER.EDIT';
    $scope.breadcrumbs.path = [{title: 'USER.LIST', url: str + 'list'}];

    var Form = new AdminDefaultUserForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminDefaultUserEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.User').controller('AdminDefaultUserEditCtrl', AdminDefaultUserEditCtrl);