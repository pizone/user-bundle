function LoginCtrl($scope, SessionService, $sessionStorage, $timeout, $http){
    $scope.username = null;
    $scope.password = null;
    $scope.error = 'ACCOUNT.ERROR_LOGIN';
    $scope.valid = true;
    $scope.remember = null;
    $scope.formId = 'formId';
    $scope.Click = {
        Submit: Submit,
        ForgotPassword: ForgotPassword
    };

    function Init(){
        if($sessionStorage.isAuthorized) {
            if(SessionService.CheckCredentials("ROLE_SUPER_ADMIN"))
                $scope.$state.go('content.content_list');
            else if(SessionService.CheckCredentials("ROLE_ADMIN"))
                $scope.$state.go('content.content_list');
            else if(SessionService.CheckCredentials("ROLE_MANAGER"))
                $scope.$state.go('shop.order_list');
            else
                $scope.$state.go('login');
        }
        else{
            checkAurhorized();
        }
    }

    function checkAurhorized(){
        $.ajax({
            url: PiZoneConfig.apiPrefix + '/login',
            type: 'post',
            dataType: 'json',
            success: function(message){
                if(message.result == 'ok') {
                    SessionService.SetCredentials(null, null, message.roles);
                    if(SessionService.CheckCredentials("ROLE_SUPER_ADMIN"))
                        $scope.$state.go('content.content_list');
                    else if(SessionService.CheckCredentials("ROLE_ADMIN"))
                        $scope.$state.go('content.content_list');
                    else if(SessionService.CheckCredentials("ROLE_MANAGER"))
                        $scope.$state.go('shop.order_list');
                    else
                        $scope.$state.go('login');
                }
            }
        });
    }

    function Submit(){
        var container = $('#' + $scope.formId);
        var loader = new Loader(container);
        $.ajax({
            url: PiZoneConfig.apiPrefix + '/login',
            type: 'post',
            data: getValues(),
            dataType: 'json',
            beforeSend: function(){
                if(loader.ready === false)
                    loader.Create();
            },
            error: function(message) {
                $timeout(function(){
                    $scope.valid = false;
                });

                loader.Delete();
            },
            success: function(message){
                if(message.result == 'ok') {
                    $scope.valid = true;
                    SessionService.SetCredentials($scope.username, $scope.password, message.roles);
                    if(SessionService.CheckCredentials("ROLE_SUPER_ADMIN"))
                        $scope.$state.go('content.content_list');
                    else if(SessionService.CheckCredentials("ROLE_ADMIN"))
                        $scope.$state.go('content.content_list');
                    else if(SessionService.CheckCredentials("ROLE_MANAGER"))
                        $scope.$state.go('shop.order_list');
                    else
                        $scope.$state.go('login');
                }
                else{
                    $timeout(function() {
                        $scope.valid = false;
                    });
                    loader.Delete();
                }
            }
        });
    }

    function getValues(){
        return $('#' + $scope.formId).serialize();
    }

    function ForgotPassword(){

    }

    Init();
}
LoginCtrl.$inject = ['$scope', 'SessionService', '$sessionStorage', '$timeout', '$http'];
angular.module('PiZone.User').controller('LoginCtrl', LoginCtrl);
