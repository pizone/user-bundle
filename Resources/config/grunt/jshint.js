module.exports = {
    options: {
        reporter: require('jshint-stylish')
    },
    main: [
        'web/bundles/pizoneuser/js/**/*.js'
    ]
};