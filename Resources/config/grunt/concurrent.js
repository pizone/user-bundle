module.exports = {
    // Опции
    options: {
        limit: 3
    },

    // Задачи разработки
    devFirstUser: [
        'jshint'
    ],
    devSecondUser: [
        'uglify'
    ],


    // Производственные задачи
    prodFirstUser: [
        'jshint'
    ],
    prodSecondUser: [
        'uglify'
    ]
};