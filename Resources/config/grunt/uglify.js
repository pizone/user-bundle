module.exports = {
    my_target: {
        files: {
            'web/assetic/js/user.min.js': [
                'web/bundles/pizoneuser/js/app.js',
                'web/bundles/pizoneuser/js/translations/**/*.js',
                'web/bundles/pizoneuser/js/translations.js',
                // 'web/bundles/pizoneuser/js/config.js',
                'web/bundles/pizoneuser/js/controller/**/*.js',
                'web/bundles/pizoneuser/js/model/**/*.js',
                'web/bundles/pizoneuser/js/factory/*.js',
                'web/bundles/pizoneuser/js/module/*.js'
            ]
        }
    }
};