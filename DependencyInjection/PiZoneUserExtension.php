<?php

namespace PiZone\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class PiZoneUserExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->setAllParameters($container, $config, $this->getAlias());

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $orm = array();

        foreach ($config['orm'] as $name => $listeners) {
            $orm[$name] = array(
                'connection' => $name,
                'class' => $listeners['class']
            );
            if(isset($listeners['url_prefix'])) {
                foreach($listeners['url_prefix'] as $one) {
                    $orm[$name]['url_prefix'][] = quotemeta($one);
                }
            }
        }

        $container->setParameter('pz_user.orm', $orm);
    }

    private  function setAllParameters($container, $config, $prefix){
        foreach ($config as $key => $value) {
            if(is_array($value)){
                $this->setAllParameters($container, $value, $prefix.'.'.$key);
            }
            else
                $container->setParameter($prefix.'.'.$key, $value);
        }
    }
}
