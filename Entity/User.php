<?php

namespace PiZone\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\AttributeOverride;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="pz_account")
 * @UniqueEntity(fields="usernameCanonical", errorPath="username", message="fos_user.username.already_used", groups={"User"})
 */
class User extends BaseUser
{
    /**
     * @var $id integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Date registration
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $registration_at;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\FormBundle\Entity\FormComment", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $form_comment;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\FormBundle\Entity\FormStatus", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $form_status;

    /**
     * @ORM\ManyToMany(targetEntity="PiZone\FormBundle\Entity\Form", inversedBy="users")
     * @ORM\JoinTable(name="pz_form_user",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="form_id", referencedColumnName="id")}
     * )
     */
    protected $forms;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set registrationAt
     *
     * @param \DateTime $registrationAt
     *
     * @return User
     */
    public function setRegistrationAt($registrationAt)
    {
        $this->registration_at = $registrationAt;

        return $this;
    }

    /**
     * Get registrationAt
     *
     * @return \DateTime
     */
    public function getRegistrationAt()
    {
        return $this->registration_at;
    }
}
