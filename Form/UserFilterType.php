<?php

namespace PiZone\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'USER.FIELD.ALL',
                'required' => false
            ))
            ->add('username', TextType::class, array(
                'label' => 'USER.FIELD.USERNAME',
                'required' => false
            ))
            ->add('email', TextType::class, array(
                'label' => 'USER.FIELD.EMAIL',
                'required' => false
            ))
            ->add('enabled',  ChoiceType::class, array(
                'choices' =>   array('USER.FIELD.ENABLED.NO' => 0,    'USER.FIELD.ENABLED.YES' => 1,),
                'label' => 'USER.FIELD.ENABLED.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'USER.FIELD.ENABLED.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add(
                $builder->create('registration_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'USER.FIELD.REGISTRATION_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add(
                $builder->create('last_login', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'USER.FIELD.LAST_LOGIN'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
        ;
    }
}
