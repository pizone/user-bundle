<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PiZone\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager as BaseUserManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserManager extends BaseUserManager
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    /**
     * Constructor.
     *
     * @param PasswordUpdaterInterface $passwordUpdater
     * @param CanonicalFieldsUpdater   $canonicalFieldsUpdater
     * @param ObjectManager            $om
     * @param string                   $account
     * @param string                   $user
     * @param ContainerInterface $container
     */
    public function __construct(PasswordUpdaterInterface $passwordUpdater, CanonicalFieldsUpdater $canonicalFieldsUpdater, ObjectManager $om, $account, $user, $container)
    {
        parent::__construct($passwordUpdater, $canonicalFieldsUpdater);

        $orm = $container->getParameter('pz_user.orm');
        $path = null;
        $request = $container->get('request_stack')->getCurrentRequest();
        if($request)
            $path = $request->getPathInfo();

        $defManager = 'default';
        $defClass = $container->getParameter('pi_zone_user.orm.default.class');

        $manager = null;
        $class = null;

        if($request) {
            foreach ($orm as $one) {
                if (isset($one['url_prefix'])) {
                    foreach ($one['url_prefix'] as $prefix) {
                        $length = strlen($prefix);
                        if (substr($path, 0, $length) == $prefix) {
                            $manager = $one['connection'];
                            $class = $one['class'];
                        }
                    }
                } else {
                    $defManager = $one['connection'];
                    $defClass = $one['class'];
                }
            }

            if ($manager) {
                $defManager = $manager;
                $defClass = $class;
            }
        }

        $em = $container->get('doctrine')->getManager($defManager);
        $this->objectManager = $em;

        $this->repository = $em->getRepository($defClass);

        $metadata = $em->getClassMetadata($defClass);

        $this->class = $metadata->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function deleteUser(UserInterface $user)
    {
        $this->objectManager->remove($user);
        $this->objectManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function findUserBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findUsers()
    {
        return $this->repository->findAll();
    }

    /**
     * {@inheritdoc}
     */
    public function reloadUser(UserInterface $user)
    {
        $this->objectManager->refresh($user);
    }

    /**
     * {@inheritdoc}
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        $this->objectManager->persist($user);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }
}
